//SOAL 1
console.log("// SOAL 1");

const luas = (p, l) => {
    let luaspp = p * l;
    let kelilingpp = 2 * (p + l);
    const datapp = "Panjang : " + p + " cm\nLebar : " + l + " cm";
    let cetakLuas = "\nMaka luas persegi panjang adalah " + p + " cm x " + l + " cm = " + luaspp + "cm2\u00B2";
    let cetakKeliling = "\nMaka keliling persegi panjang adalah 2x(" + p + "cm + " + l + "cm) = " + kelilingpp + "cm";
    return datapp + cetakLuas + cetakKeliling;

};
console.log(luas(2, 3));

// SOAL 2
console.log("\n" + "// SOAL 2");



const newFunction = (firstName, lastName) => {
    const namaLengkap = firstName + " " + lastName
    return console.log(namaLengkap);
}

//Driver Code 
newFunction("William", "Imoh");

//SOAL 3
console.log("\n" + "// SOAL 3");

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {
    firstName,
    lastName,
    address,
    hobby
} = newObject

// Driver code
console.log(firstName, lastName, address, hobby)

// SOAL 4
console.log("\n// SOAL 4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [west, east]
    //Driver Code
console.log(combined)

//SOAL 5
console.log("\n" + "// SOAL 5")
const planet = "earth"
const view = "glass"
var before = `Lorem ${planet} dolor sit amet, consectetur adipiscing elit, ${view}`
console.log(before)