// SOAL 1
console.log("// SOAL 1");
var nilai = 85;
if (nilai >= 85) {
    console.log("Nilai Anda : " + nilai + " Indeksnya A");
} else if (nilai >= 75 && nilai < 85) {
    console.log("Nilai Anda : " + nilai + "Indeksnya B");
} else if (nilai >= 65 && nilai < 75) {
    console.log("Nilai Anda : " + nilai + "Indeksnya C");
} else if (nilai >= 55 && nilai < 65) {
    console.log("Nilai Anda : " + nilai + "Indeksnya D");
} else {
    console.log("Nilai Anda : " + nilai + "Indeksnya E");
}

// SOAL 2
console.log("\n" + "// SOAL 2");
var tanggal = 6;
var bulan = 11;
var tahun = 1992;
switch (bulan) {
    //bulan = 1;
    case 1:
        {
            console.log("Tanggal Lahir Anda : " + tanggal + " Januari " + tahun);
            break;
        }
        //bulan = 2;
    case 2:
        {
            console.log("Tanggal Lahir Anda : " + tanggal + " Februari " + tahun);
            break;
        }

        //bulan = 3;
    case 3:
        {
            console.log("Tanggal Lahir Anda : " + tanggal + " Maret " + tahun);
            break;
        }
        //bulan = 4;
    case 4:
        {
            console.log("Tanggal Lahir Anda : " + tanggal + " April " + tahun);
            break;
        }

        //bulan = 5;
    case 5:
        {
            console.log("Tanggal Lahir Anda : " + tanggal + " Mei " + tahun);
            break;
        }
        //bulan = 6;
    case 6:
        {
            console.log("Tanggal Lahir Anda : " + tanggal + " Juni " + tahun);
            break;
        }
        //bulan = 7;
    case 7:
        {
            console.log("Tanggal Lahir Anda : " + tanggal + " Juli " + tahun);
            break;
        }
        //bulan = 8;
    case 8:
        {
            console.log("Tanggal Lahir Anda : " + tanggal + " Agustus " + tahun);
            break;
        }
        //bulan = 9;
    case 9:
        {
            console.log("Tanggal Lahir Anda : " + tanggal + " September " + tahun);
            break;
        }
        //bulan = 10;
    case 10:
        {
            console.log("Tanggal Lahir Anda : " + tanggal + " Oktober " + tahun);
            break;
        }
        //bulan = 11;
    case 11:
        {
            console.log("Tanggal Lahir Anda : " + tanggal + " November " + tahun);
            break;
        }
        //bulan = 12;
    case 12:
        {
            console.log("Tanggal Lahir Anda : " + tanggal + " Desember " + tahun);
            break;
        }
}

// SOAL 3
console.log("\n" + "// SOAL 3");
var n = 1;
console.log("n = " + n);
while (n <= 3) {
    console.log('#'.repeat(n));
    n++;
}

var n = 1;
console.log("n = 7");
while (n <= 7) {
    console.log('#'.repeat(n));
    n++;
}

// SOAL 4
console.log("\n" + "// SOAL 4");
console.log('Output untuk m = 3');
var cinta;
for (let m = 1; m <= 3; m++) {
    if (m == 1) {
        cinta = "programming";
    } else if (m == 2) {
        cinta = "javascript";
    } else {
        cinta = "VueJS";
    }
    console.log(m + " - I Love " + cinta);
    if (m == 3) {
        console.log('#'.repeat(m));
    }
}

console.log("\n" + 'Output untuk m = 7');
var cinta;
var counter = 1;
for (let m = 1; m <= 7; m++) {
    if (counter == 1) {
        cinta = "programming";
    } else if (counter == 2) {
        cinta = "javascript";
    } else {
        counter = 1;
        cinta = "VueJS";
    }
    console.log(m + " - I Love " + cinta);
    if (m % 3 == 0) {
        counter == 0;
        console.log('#'.repeat(m));
    } else {
        counter++;
    }
}


console.log("\n" + 'Output untuk m = 10');
var cinta;
var counter = 1;
for (let m = 1; m <= 10; m++) {
    if (counter == 1) {
        cinta = "programming";
    } else if (counter == 2) {
        cinta = "javascript";
    } else {
        counter = 1;
        cinta = "VueJS";
    }
    console.log(m + " - I Love " + cinta);
    if (m % 3 == 0) {
        counter == 0;
        console.log('#'.repeat(m));
    } else {
        counter++;
    }
}