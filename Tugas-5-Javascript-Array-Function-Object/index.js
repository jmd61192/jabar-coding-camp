// SOAL 1
console.log("// SOAL 1");
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
var sortDaftarHewan = daftarHewan.sort();
for (i = 0; i <= sortDaftarHewan.length - 1; i++) {
    console.log(sortDaftarHewan[i])
}
// SOAL 2
console.log("\n" + "// SOAL 2");

function introduce(data) {
    var tampilIntroduce = "Nama saya " +
        data.name + ", umur saya " +
        data.age + " tahun, alamat saya di " +
        data.address + ", dan saya punya hobby yaitu " + data.hobby
    return tampilIntroduce;
}
var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

// SOAL 3
console.log("\n" + "// SOAL 3");
const huruVokal = ["a", "i", "u", "e", "o"]

function hitung_huruf_vokal(data) {
    var dataLower = data.toLowerCase();
    var dataSplit = dataLower.split("");
    var counter = 0;
    for (i = 0; i <= dataSplit.length - 1; i++) {
        if (huruVokal.includes(dataSplit[i])) {
            counter++;
        }
    }
    return counter;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2) // 3 2

// SOAL 4
console.log("\n" + "// SOAL 4");

function hitung(input) {
    var output = [-2, 0, 2, 4, 6, 8];
    return output[input];
}

console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8