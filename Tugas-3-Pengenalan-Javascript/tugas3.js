//Soal 1
console.log("// Soal 1");
//definiskan variable soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var newPertama = pertama.substring(0, 5) + pertama.substring(12, 19);
var newKedua = kedua.substring(0, 8) + kedua.substring(8, 18).toUpperCase();
//definisi variable untuk memunculin ke HTML element
var jawabanSoal1 = newPertama.concat(newKedua);

var tempelJawaban1 = document.getElementById("jawaban1");
//menampilkan hasil di console browser
console.log(jawabanSoal1);
// menampilkan output ke elemen jawaban1
tempelJawaban1.innerHTML = "<p>" +
    jawabanSoal1 + "</p>";

//Soal 2
console.log("// Soal 2");

//definiskan variable soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

//Ubah string ke Ineteger
var newKataPertama = parseInt(kataPertama);
var newKataKedua = parseInt(kataKedua);
var newKataKetiga = parseInt(kataKetiga);
var newKataKeempat = parseInt(kataKeempat);

//definisi variable untuk memunculin ke HTML element
var tempelJawaban2 = document.getElementById("jawaban2");

//lakukan perhitungan
var hitung = newKataPertama + newKataKedua * newKataKetiga + newKataKeempat;

//menampilkan hasil di console browser
console.log("Jawaban Soal 2 " + hitung);

// menampilkan output ke elemen jawaban1
tempelJawaban2.innerHTML = "<p>" +
    hitung + "</p>";

//Soal 3
console.log("// Soal 3");
//definiskan variable soal
var kalimat = 'wah javascript itu keren sekali';

//Mengembalikan potongan string mulai dari indeks pada parameter pertama (indeks awal) sampai dengan indeks pada parameter kedua (indeks akhir).
var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

//definisi variable untuk memunculin ke HTML element
var tempelJawaban3 = document.getElementById("jawaban3");

//menampilkan hasil di console browser
console.log("Jawaban Soal 3 ");
console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);

// menampilkan output ke elemen jawaban1
tempelJawaban3.innerHTML = "<p> Kata Pertama: " + kataPertama + "<br>" +
    "<p> Kata Kedua: " + kataKedua + "<br>" +
    "<p> Kata Ketiga: " + kataKetiga + "<br>" +
    "<p> Kata Keempat: " + kataKeempat + "<br>" +
    "<p> Kata Kelima: " + kataKelima

    +
    "</p>";