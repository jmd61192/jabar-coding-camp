// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
];
// Tulis code untuk memanggil function readBooks di sini
const lookupBook = (remainingWaktu, books, index) => {
        readBooks(remainingWaktu, books[index], function(waktu) {
            const bukuSelanjutnya = index + 1;
            if (bukuSelanjutnya < books.length) {
                lookupBook(waktu, books, bukuSelanjutnya);
            }
        });
    }
    // soal 1
console.log("// SOAL 1")
lookupBook(10000, books, 0);